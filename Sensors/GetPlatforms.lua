local sensorInfo = {
	name = "GetPlatforms",
	desc = "Returns descriptions (center, width, height) of platforms on the whole map, sorted by distance from start point.",
	author = "Michelle",
	date = "2022-05-13",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetGroundHeight = Spring.GetGroundHeight

-- finds area of the same terrain height around the given point
-- returns its center and size (widht, height)
function GetPlatformCenterAndSize(x, y, z)
    local mapX = Game.mapSizeX
    local mapZ = Game.mapSizeZ
    -- go to the left
    local xMin = x
    while GetGroundHeight(xMin, z) >= y and xMin > 0 do
        xMin = xMin - 1
    end
    -- go to the right
    local xMax = x
    while GetGroundHeight(xMax, z) >= y and xMax <= mapX do
        xMax = xMax + 1
    end
    -- go up
    local zMin = z
    while GetGroundHeight(x, zMin) >= y and zMin > 0 do
        zMin = zMin - 1
    end
    -- go down
    local zMax = z
    while GetGroundHeight(x, zMax) >= y and zMax <= mapZ do
        zMax = zMax + 1
    end
    -- compute center and size and return
    local width = xMax - xMin - 1
    local height = zMax - zMin - 1
    local centerX = xMin + math.floor(width/2) + 1  -- for even width wil get the right one
    local centerZ = zMin + math.floor(height/2) + 1  -- for even height will get the bottom one
    local center = Vec3(centerX, GetGroundHeight(centerX, centerZ), centerZ)
    return center, width, height
end

-- returns true if the center corresponds to already stored platform
-- to avoid duplicities in the platforms array
function IsAlreadySeenPlatform(center, platforms)
    for _, platform in ipairs(platforms.unarmored) do
        local otherCenter = platform.center
        if center == otherCenter then return true end
    end
    for _, platform in ipairs(platforms.armored) do
        local otherCenter = platform.center
        if center == otherCenter then return true end
    end
    return false
end

function IsArmoredPlatform(center, enemy_positions, enemy_range)
    -- would be better to take into account the enemy unit's range (rather than a constant)
    --      but I don't have this information until I know, what unit is there
    for _, position in ipairs(enemy_positions) do
        if position:Distance(center) < enemy_range then
            return true
        end
    end
    return false
end

-- sorts the given array of paltforms according to the distance to the given point
--     ineffective, but I don't expect a lot of platforms
function SortByDistance(platforms, point)
    -- simply find minimum, move it to the end of the new array and repeat until the original array is empty
    local sorted_platforms = {}
    while #platforms > 0 do
        -- find minimum
        local minDist = math.huge
        local minIndex = 1
        for index, platform in ipairs(platforms) do
            local dist = platform.center:Distance(point)
            if dist < minDist then
                minDist = dist
                minIndex = index
            end
        end
        -- remove minimum and add it to the new array
        sorted_platforms[#sorted_platforms+1] = table.remove(platforms, minIndex)
    end
    return sorted_platforms
end

-- @description returns descriptions (center, width, height) of platforms on the whole map, sorted by distance from start point
--     step - should be around 120 or even less
--     height_threshold - should be core.MissionInfo().areaHeight
--     enemy_positions - should be core.MissionInfo().enemy_positions
--     enemy_range - range of enemy attack to decide whether a platform is armored or not
return function(start_point, step, height_threshold, enemy_positions, enemy_range)
    local maxX = Game.mapSizeX
    local maxZ = Game.mapSizeZ

    local platforms = {
        unarmored = {},
        armored = {}
    }

    -- scan the whole map for platforms (defined by their height greater or equal to the given threshold)
    for x=1, maxX, step do
        for z=1, maxZ, step do
            local y = GetGroundHeight(x, z)
            if y == height_threshold then  -- it is a position on the platform
                -- find center and size of the platform
                local center, width, height = GetPlatformCenterAndSize(x, y, z)
                -- decide whether it is previously seen platform or not
                if not IsAlreadySeenPlatform(center, platforms) then  -- not already seen
                    -- decide whether it is armored or not and store it in a corresponding array
                    if IsArmoredPlatform(center, enemy_positions, enemy_range) then
                        platforms.armored[#platforms.armored+1] = {
                            center = center, width = width, height = height
                        }
                    else
                        platforms.unarmored[#platforms.unarmored+1] = {
                            center = center, width = width, height = height
                        }
                    end
                end
            end
        end
    end

    -- sort the platforms according to the distance
    local result = {
        unarmored = SortByDistance(platforms.unarmored, start_point),
        armored = SortByDistance(platforms.armored, start_point)
    }

    -- return the result
    return result

end