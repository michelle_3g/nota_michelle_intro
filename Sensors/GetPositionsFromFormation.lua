local sensorInfo = {
	name = "GetReadyUnits",
	desc = "Returns an array (of length N) of absolute positions derived from the relative formation and the destination.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns an array (of length N) of absolute positions derived from the relative formation and the destination
return function(relativeFormation, destination, N)
    local positions = {}

    for index, relativePosition in ipairs(relativeFormation) do
        if index > N then break end
        positions[#positions+1] = destination + relativePosition
    end

    return positions
end