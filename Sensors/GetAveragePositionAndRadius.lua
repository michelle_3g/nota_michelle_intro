local sensorInfo = {
	name = "GetAveragePosition",
	desc = "Returns average position of the given units and the radius they are in",
	author = "Michelle",
	date = "2022-05-13",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetUnitPosition = Spring.GetUnitPosition

-- @description reeturns average position of the given units and the radiud they are in
return function(units)
	local result = {
		position = Vec3(0, 0, 0),
		radius = 0
	}

    local sumPosition = Vec3(0, 0, 0)
    
	-- sum up positions of all the units
	for _, unitID in ipairs(units) do
        sumPosition = sumPosition + Vec3(GetUnitPosition(unitID))
	end
    -- compute average position
    result.position = sumPosition / #units

	-- take the largest distance of unit from the average as a radius
	for _, unitID in ipairs(units) do
		local dist = result.position:Distance(Vec3(GetUnitPosition(unitID)))
		if dist > result.radius then
			result.radius = dist
		end
	end

	return result
end