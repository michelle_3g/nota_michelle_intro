local sensorInfo = {
	name = "AllAtlasesDead",
	desc = "Returns true if all Atlases are dead, false otherwise.",
	author = "Michelle",
	date = "2022-06-14",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return true if all Atlases are dead, false otherwise
return function(atlases)
    for _, atlasID in ipairs(atlases) do
        local unitIsDead = UnitIsDead(atlasID)
        -- if there is an Atlas which is not dead, return false
        if unitIsDead ~= nil and unitIsDead ~= true then return false end
    end
    -- otherwise everyone is dead
    return true
end