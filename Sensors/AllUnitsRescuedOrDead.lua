local sensorInfo = {
	name = "AllUnitsRescuedOrDead",
	desc = "Returns true if all the units are either safe or dead, false otherwise.",
	author = "Michelle",
	date = "2022-06-14",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return true if all the units are safe, false otherwise
return function()
    -- go through units in the rescue manager
    for _, unitStatus in pairs(bb.rescueManager.unitToRescueStatus) do
        if unitStatus ~= "safe" and unitStatus ~= "dead" then return false end
    end
    return true
end