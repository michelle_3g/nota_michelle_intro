local sensorInfo = {
	name = "GetSpyPositions",
	desc = "Returns an array of positions for infiltrator deployment.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitRotation = Spring.GetUnitRotation

-- @description return an array of positions for infiltrator deployment
return function(infiltrators, path)
    local result = {}
	
    -- choose last N points on the path (where N is the number of infiltrators)
    for i, _ in ipairs(infiltrators) do
        result[#result+1] = path[#path-i+1]
    end

    return result

end