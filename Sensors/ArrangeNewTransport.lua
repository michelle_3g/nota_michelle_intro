local sensorInfo = {
	name = "ArrangeNewTransport",
	desc = "Returns a pair of an available Atlas and a unit waiting to be transported.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- speed-ups
local GetUnitHealth = Spring.GetUnitHealth
local GetUnitPosition = Spring.GetUnitPosition

-- @description return  pair of an available Atlas and a unit waiting to be transported
return function()
    -- initialize the pair with default values
    local transportInfo = {
        atlas = nil,
        unitToTransport = nil
    }

    -- find an available Atlas
    for _, atlasID in ipairs(bb.units.atlases) do
        local atlasStatus = bb.units.status[atlasID]
        if atlasStatus == "available" then
            transportInfo.atlas = atlasID
            break
        end
    end

    -- find a unit among infiltrators - they have priority
    for _, unitID in ipairs(bb.units.infiltrators) do
        local unitStatus = bb.units.status[unitID]
        if unitStatus == "spawned" then
            transportInfo.unitToTransport = unitID
            break
        end
    end
    if transportInfo.unitToTransport == nil then
        -- find a unit among mavericks
        for _, unitID in ipairs(bb.units.mavericks) do
            local unitStatus = bb.units.status[unitID]
            if unitStatus == "spawned" then
                transportInfo.unitToTransport = unitID
                break
            end
        end
    end
    if transportInfo.unitToTransport == nil then
        -- find a unit among lugers
        for _, unitID in ipairs(bb.units.lugers) do
            local unitStatus = bb.units.status[unitID]
            if unitStatus == "spawned" then
                transportInfo.unitToTransport = unitID
                break
            end
        end
    end

    -- if there is a suitable pair change status of the units
    if transportInfo.atlas ~= nil and transportInfo.unitToTransport ~= nil then
        bb.units.status[transportInfo.atlas] = "occupied"
        bb.units.status[transportInfo.unitToTransport] = "transported"
    end

    -- return the pair
    return transportInfo

end