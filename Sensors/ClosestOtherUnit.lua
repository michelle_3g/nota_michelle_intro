local sensorInfo = {
	name = "ClosestOtherUnit",
	desc = "Returns distance between the commander and the closest other unit",
	author = "Michelle",
	date = "2022-04-19",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitPosition = Spring.GetUnitPosition

-- @description return distance to the closest unit from the battle commander
return function()
    local from = Vec3(0,0,0)
    local minDist = math.huge
    -- find the commander
	for i = 1, #units do
		local unitID = units[i]
		if UnitDefs[GetUnitDefID(unitID)].name == "armbcom" then
            from = Vec3(GetUnitPosition(unitID))
        end
    end
    -- find the closest other unit
    for i = 1, #units do
        local unitID = units[i]
		if UnitDefs[GetUnitDefID(unitID)].name ~= "armbcom" then
            local to = Vec3(GetUnitPosition(unitID))
            local diff = to - from
            local dist = math.sqrt(diff.x*diff.x + diff.y*diff.y + diff.z * diff.z)
            if dist < minDist then
                minDist = dist
            end
        end
    end
	
    return minDist
end