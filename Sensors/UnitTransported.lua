local sensorInfo = {
	name = "UnitTransported",
	desc = "Changes status of the given pair (Atlas, unitToTransport).",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GetUnitTransporter = Spring.GetUnitTransporter

-- @description changes status of the given pair (Atlas, unitToTransport)
return function(transporter, unitToTransport)
    bb.units.status[transporter] = "returning"
    bb.units.status[unitToTransport] = "ready"

    return nil
end