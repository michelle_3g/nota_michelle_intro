local sensorInfo = {
	name = "GetPlatformsCoordinates",
	desc = "Returns positions of the the given platforms' centers (platforms are represented as in GetPlatforms senzor).",
	author = "Michelle",
	date = "2022-05-11",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns positions of the the given platforms' centers
return function(platforms)
    local result = {}
    for _, platform in ipairs(platforms) do
        result[#result+1] = platform.center
    end

    return result

end