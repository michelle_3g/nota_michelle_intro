local sensorInfo = {
	name = "FindBehindEnemyBase",
	desc = "Returns a point which is far behind an enemy base.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local DISTANCE_FROM_EDGE = 200

-- speed-ups
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitPosition = Spring.GetUnitPosition

-- @description return a point which is far behind an enemy base
return function(myBasePosition, enemyBasePosition)
    -- get direction from myBasePosition to EnemyBasePosition
    local direction = (enemyBasePosition - myBasePosition):GetNormalized()
    -- go in that direction from the enemy base for a sufficient distance
    local result = enemyBasePosition + direction * 1500
    -- don't overshoot
    local maxX = Game.mapSizeX
    local maxZ = Game.mapSizeZ
    if result.x < DISTANCE_FROM_EDGE then result.x = DISTANCE_FROM_EDGE end
    if result.x > maxX-DISTANCE_FROM_EDGE then result.x = maxX-DISTANCE_FROM_EDGE end
    if result.z < DISTANCE_FROM_EDGE then result.z = DISTANCE_FROM_EDGE end
    if result.z > maxZ-DISTANCE_FROM_EDGE then result.z = maxZ-DISTANCE_FROM_EDGE end
    -- return the result
    return result
end