local sensorInfo = {
	name = "AllAtlasesDead",
	desc = "Returns true if we have enough metal to purchase the given item (unit / line upgrade), false otherwise",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return true if we have enough metal to purchase the given item (unit / line upgrade), false otherwise
return function(unitName, lineName, lineUpgradeLevel)
    local teamID = Spring.GetMyTeamID()
    local currentMetal, _, _, _, _, _, _, _ = Spring.GetTeamResources(teamID, "metal")
    if unitName ~= nil then
        local cost = bb.missionInfo.buy[unitName]
        if cost ~= nil and cost <= currentMetal then
            return true
        end
        return false
    elseif lineName ~= nil and lineUpgradeLevel ~= nil then
        if lineUpgradeLevel >= 15 then lineUpgradeLevel = 15 end
        local cost = bb.missionInfo.upgrade.line
        if cost ~= nil and cost * lineUpgradeLevel <= currentMetal then
            return true
        end
        return false
    end
    -- otherwise the parameters are not valid
    return false
end