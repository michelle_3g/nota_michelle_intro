local sensorInfo = {
	name = "GetReadyUnits",
	desc = "Returns an array of units ready for combat.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns an array of units ready for combat
return function()
    local readyUnits = {}
    
    local i = 1
    -- get ready Mavericks
    for index, unitID in ipairs(bb.units.mavericks) do
        local status = bb.units.status[unitID]
        if status == "ready" then
            readyUnits[#readyUnits+1] = unitID
        end
    end
    -- get ready Lugers
    for index, unitID in ipairs(bb.units.lugers) do
        local status = bb.units.status[unitID]
        if status == "ready" then
            readyUnits[#readyUnits+1] = unitID
        end
    end

    return readyUnits
end