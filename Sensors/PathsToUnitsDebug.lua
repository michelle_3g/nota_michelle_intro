local sensorInfo = {
	name = "PathsToUnitsDebug",
	desc = "Displays paths",
	author = "Michelle",
	date = "2022-06-13",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description display paths
return function(paths)
    if (Script.LuaUI('exampleDebug_update')) then
        local ID = 1
        for unitID, path in pairs(paths) do
            for i=1, #path-1 do
                Script.LuaUI.exampleDebug_update(
                    ID, -- key
                    {	-- data - starting and ending point of the line
                        startPos = path[i],
                        endPos = path[i+1],
                        color = { 0.2, 0.5, 0.9 },
                        line_width = 2
                    }
                )
                ID = ID + 1
            end
        end
        
    end
    return nil
end