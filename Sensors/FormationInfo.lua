local sensorInfo = {
	name = "FormationInfo",
	desc = "Returns formation information for the selected units",
	author = "Michelle",
	date = "2022-04-19",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitRotation = Spring.GetUnitRotation

-- @description return formation description
return function()
	local form = {
		isCommander = false,
		groupDefinition = {},
		position = Vec3(0,0,0),
		formation = {}
	}

	local index = 1
	local rotation = 0
	for i = 1, #units do
		local unitID = units[i]
		if UnitDefs[GetUnitDefID(unitID)].name == "armbcom" then  -- battle commander - store position and rotation
			form.position = Vec3(GetUnitPosition(unitID))
			_,rotation,_ = GetUnitRotation(unitID)
			form.isCommander = true
		else  -- other - store unitID and index
			form.groupDefinition[unitID] = index
			index = index + 1
		end
	end

	rotation = rotation + 180  -- rotation behind the commander

	-- place other units behind the commander, cover a square in a regular grid
	local i = 0
	local side = math.ceil(math.sqrt(index - 1))  -- number of units on a side of a square
	local spacing = 30  -- space between adjacent units
	local half = (side - 1) * spacing / 2  -- half of the square's side
	form.position = form.position - Vec3(20 + (side - 1) * spacing, 0, half)  -- position of the "top-left" unit derived from the commander's position
	for unitID, unitIndex in pairs(form.groupDefinition) do
		-- compute a point on a grid
		form.formation[unitIndex] = Vec3(spacing * (i / side), 0, spacing * (i % side)):Rotate2D(rotation * 180/3.14159) -- degrees to radians
		i = i + 1
	end

	return form
end