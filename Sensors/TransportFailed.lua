local sensorInfo = {
	name = "TransportFailed",
	desc = "Changes status of the given pair (Atlas, unitToTransport) according to the cause of failure.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GetUnitTransporter = Spring.GetUnitTransporter

-- @description change status of the given pair (Atlas, unitToTransport) according to the cause of failure
return function(transportInfo, success)
    -- change status of the atlas
    -- if it is dead --> dead
    local unitIsDead = UnitIsDead(transportInfo.atlas)
    if unitIsDead == true or unitIsDead == nil then
        bb.units.status[transportInfo.atlas] = "dead"
    -- if it is alive and not occupied --> available
    elseif #GetUnitIsTransporting(transportInfo.atlas) == 0 then
        bb.units.status[transportInfo.atlas] = "available"
    end

    -- change status of the unit to transport
    -- if it is dead --> dead
    unitIsDead = UnitIsDead(transportInfo.unitToTransport)
    if unitIsDead == true or unitIsDead == nil then
        bb.units.status[transportInfo.unitToTransport] = "dead"
    else
        -- if the transport mission was not successful but the unit survived, just mark it again as spawned
        bb.units.status[transportInfo.unitToTransport] = "spawned"
    end



end