local sensorInfo = {
	name = "InitializeRescueManager",
	desc = "Initializes reservation system for rescue of the given units.",
	author = "Michelle",
	date = "2022-06-14",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description initialize reservation system for rescue of the given units
return function(atlases, unitsToRescue, safeArea)
    if bb.rescueManager == nil then
        -- initialize the reservation system's data structure
        bb.rescueManager = {
            safeArea = safeArea,
            atlasStatus = {},  -- available/occupied/dead
            unitToRescueStatus = {},  -- on_battlefield/being_rescued/safe/dead
            safePositions = {},
            positionStatus = {},  -- free/reserved/occupied
            positionOffset = 0
        }
        -- compute all safe positions and initialize them as free
        local points = math.ceil(math.sqrt(#unitsToRescue))
        local squareSize = math.sqrt(safeArea.radius/2 * safeArea.radius/2 * 2)
        local cellSize = squareSize / (points - 1)
        bb.rescueManager.positionOffset = cellSize
        local topLeft = safeArea.center - Vec3(squareSize/2, 0, squareSize/2)
        for i = 0, points do
            for j = 0, points do
                bb.rescueManager.safePositions[#bb.rescueManager.safePositions+1] = Vec3(topLeft.x + cellSize*i, topLeft.y, topLeft.z + cellSize*j)
                bb.rescueManager.positionStatus[#bb.rescueManager.positionStatus+1] = "free"
            end
        end
    else
        -- initialize the reservation system's data structure
        bb.rescueManager.atlasStatus = nil
        bb.rescueManager.atlasStatus = {}
        bb.rescueManager.unitToRescueStatus = nil
        bb.rescueManager.unitToRescueStatus = {}
    end

    -- initialize all atlases as available
    for i = 1, #atlases do
        local atlasID = atlases[i]
        bb.rescueManager.atlasStatus[atlasID] = "available"
    end

    -- initialize all units to be rescued as not rescued
    for i = 1, #unitsToRescue do
        local unitID = unitsToRescue[i]
        bb.rescueManager.unitToRescueStatus[unitID] = "on_battlefield"
    end


    return nil
end