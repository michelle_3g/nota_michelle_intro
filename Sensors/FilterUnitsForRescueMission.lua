local sensorInfo = {
	name = "FilterUnitsForRescueMission",
	desc = "Returns a table of units filtered by category into Atlases, Peepers and others.",
	author = "Michelle",
	date = "2022-06-14",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetTeamUnits = Spring.GetTeamUnits
local GetMyTeamID = Spring.GetMyTeamID
local GetUnitDefID = Spring.GetUnitDefID

-- @description return a table of units filtered by category into Atlases, Peepers and others
return function()
    -- get all of our units
    local allUnits = GetTeamUnits(GetMyTeamID())

    -- create the final table
    local filteredUnits = {
        atlases = {},
        peepers = {},
        toRescue = {}
    }

    -- filter the units according to the category
    for i = 1, #allUnits do
        local unitID = allUnits[i]
        local unitDefID = GetUnitDefID(unitID)
        local name = UnitDefs[unitDefID].name
		if name == "armatlas" then
            filteredUnits.atlases[#filteredUnits.atlases+1] = unitID
        elseif name == "armpeep" then
            filteredUnits.peepers[#filteredUnits.peepers+1] = unitID
        elseif not UnitDefs[unitDefID].cantBeTransported then  -- only transportable units are considered for rescue
            filteredUnits.toRescue[#filteredUnits.toRescue+1] = unitID
        end
    end
	
    -- return the result
    return filteredUnits
end