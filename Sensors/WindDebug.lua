local sensorInfo = {
	name = "WindDebug",
	desc = "Shows direction of the wind",
	author = "Michelle",
	date = "2022-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetUnitPosition = Spring.GetUnitPosition
local GetWind = Spring.GetWind

-- @description return current wind direction
return function(windDebugLengthMultiplier)
	if #units > 0 then
		local unitID = units[1]
		local x,y,z = GetUnitPosition(unitID)
		if (Script.LuaUI('exampleDebug_update')) then
            local dirX, _, dirZ, _, _, _, _ = GetWind()
			Script.LuaUI.exampleDebug_update(
				unitID, -- key
				{	-- data - starting and ending point of the line
					startPos = Vec3(x,y,z), 
					endPos = Vec3(x,y,z) + Vec3(dirX, 0, dirZ) * windDebugLengthMultiplier
				}
			)
		end
		return {	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(x,y,z)
				}
	end
end