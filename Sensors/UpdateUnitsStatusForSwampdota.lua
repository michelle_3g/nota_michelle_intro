local sensorInfo = {
	name = "UpdateUnitsStatusForSwampdota",
	desc = "Updates status of units filtered by category into Atlases, Mavericks, Lugers, Infiltrators and others.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetTeamUnits = Spring.GetTeamUnits
local GetMyTeamID = Spring.GetMyTeamID
local GetUnitDefID = Spring.GetUnitDefID
local UnitIsDead = Spring.GetUnitIsDead
local ValidUnitID = Spring.ValidUnitID

-- @description update status of units filtered by category into Atlases, Mavericks, Lugers, Infiltrators and others
return function(unitsGroups)
    -- get all of our units
    local allUnits = GetTeamUnits(GetMyTeamID())

    -- filter the units according to the category
    for i = 1, #allUnits do
        local unitID = allUnits[i]
        if unitsGroups.status[unitID] == nil then  -- a newly purchased unit needs to be assigned to a category
            local unitDefID = GetUnitDefID(unitID)
            local name = UnitDefs[unitDefID].name
            if name == "armatlas" then
                unitsGroups.atlases[#unitsGroups.atlases+1] = unitID
                unitsGroups.status[unitID] = "available"
            elseif name == "armmart" then
                unitsGroups.lugers[#unitsGroups.lugers+1] = unitID
                unitsGroups.status[unitID] = "spawned"
            elseif name == "armmav" then
                unitsGroups.mavericks[#unitsGroups.mavericks+1] = unitID
                unitsGroups.status[unitID] = "spawned"
            elseif name == "armspy" then
                unitsGroups.infiltrators[#unitsGroups.infiltrators+1] = unitID
                unitsGroups.status[unitID] = "spawned"
            else
                unitsGroups.others[#unitsGroups.others+1] = unitID
                unitsGroups.status[unitID] = "spawned"
            end
        else  -- for older units check if they are alive
            if not ValidUnitID(unitID) then
                unitsGroups.status[unitID] = "dead"
            end
            local unitIsDead = UnitIsDead(unitID)
            if unitIsDead == true or unitIsDead == nil then
                unitsGroups.status[unitID] = "dead"
            end
        end
    end

    return nil
end