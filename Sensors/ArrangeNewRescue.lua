local sensorInfo = {
	name = "ArrangeNewRescue",
	desc = "Returns a triple of an available Atlas, the unit to rescue which is the closest to the safe area and a safe destination point (while changing their status in the rescue manager accordingly).",
	author = "Michelle",
	date = "2022-06-13",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- speed-ups
local GetUnitHealth = Spring.GetUnitHealth
local GetUnitPosition = Spring.GetUnitPosition

-- @description return a triple of an available Atlas, the unit to rescue which is the closest to the safe area and a safe destination point (while changing their status in the rescue manager accordingly)
return function()
    -- initialize the pair with default values
    local rescueInfo = {
        atlas = nil,
        unitToRescue = nil,
        destinationIndex = -1
    }

    -- find an available Atlas with the highest health
    local maxAtlasHealth = -1
    for atlasID, atlasStatus in pairs(bb.rescueManager.atlasStatus) do
        if atlasStatus == "available" then
            local health, maxHealth, paralyzeDamage, captureProgress, buildProgress = GetUnitHealth(atlasID)
            local lifePercentage = health/maxHealth
            if lifePercentage > maxAtlasHealth then
                maxAtlasHealth = lifePercentage
                rescueInfo.atlas = atlasID
            end
        end
    end

    -- find the closest unit which is still on the battlefield
    local minDist = math.huge
    for unitID, unitStatus in pairs(bb.rescueManager.unitToRescueStatus) do
        if unitStatus == "on_battlefield" then
            local dist = GetDistance(Vec3(GetUnitPosition(unitID)), bb.rescueManager.safeArea.center)
            if dist < minDist then
                rescueInfo.unitToRescue = unitID
                minDist = dist
            end
        end
    end
    -- if there is no such unit, try to find the closest unit which is in more dangerous area
    if rescueInfo.unitToRescue == nil then
        minDist = math.huge
        for unitID, unitStatus in pairs(bb.rescueManager.unitToRescueStatus) do
            if unitStatus == "in_danger" then
                local dist = GetDistance(Vec3(GetUnitPosition(unitID)), bb.rescueManager.safeArea.center)
                if dist < minDist then
                    rescueInfo.unitToRescue = unitID
                    minDist = dist
                end
            end
        end
    end

    -- find a free safe position as destination
    local randomIndex = math.random(#bb.rescueManager.positionStatus)  -- select a random position
    local i = randomIndex
    while bb.rescueManager.positionStatus[i] ~= "free" do  -- find the first free position starting from the random index
        i = i + 1
        if i > #bb.rescueManager.positionStatus then
            i = 1
        end
        if i == randomIndex then  -- if we went the whole round, there is no free position
            i = -1
            break
        end
    end
    rescueInfo.destinationIndex = i

    -- if there is a suitable triple change status of the units
    if rescueInfo.atlas ~= nil and rescueInfo.unitToRescue ~= nil and rescueInfo.destinationIndex ~= -1 then
        bb.rescueManager.atlasStatus[rescueInfo.atlas] = "occupied"
        bb.rescueManager.unitToRescueStatus[rescueInfo.unitToRescue] = "being_rescued"
        bb.rescueManager.positionStatus[rescueInfo.destinationIndex] = "reserved"
    end

    -- return the triple
    return rescueInfo

end