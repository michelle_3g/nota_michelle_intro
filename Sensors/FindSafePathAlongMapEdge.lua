local sensorInfo = {
	name = "FindSafePathAlongMapEdge",
	desc = "Returns an array of points going from the given start to the given target while following a map edge.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


local DISTANCE_FROM_EDGE = 150

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead
local GetGroundHeight = Spring.GetGroundHeight

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetNearestEdge(point, maxX, maxZ)
    local dist = {[1] = point.x, [2] = maxX-point.x, [3] = point.z, [4] = maxZ-point.z}  -- distances to the four edges
    local min = math.min(dist[1], dist[2], dist[3], dist[4])
    local result = Vec3(point.x, point.y, point.z)
    if dist[1] == min then result.x = DISTANCE_FROM_EDGE end
    if dist[2] == min then result.x = maxX - DISTANCE_FROM_EDGE end
    if dist[3] == min then result.z = DISTANCE_FROM_EDGE end
    if dist[4] == min then result.z = maxZ - DISTANCE_FROM_EDGE end
    result.y = GetGroundHeight(result.x, result.z)
    return result
end

function GetVerticalEdge(point, maxX, maxZ)
    local dist = {[1] = point.x, [2] = maxX-point.x, [3] = point.z, [4] = maxZ-point.z}  -- distances to the four edges
    local min = math.min(dist[1], dist[2], dist[3], dist[4])
    local result = Vec3(point.x, point.y, point.z)
    if result.x < maxX-result.x then
        result.x = DISTANCE_FROM_EDGE
    else
        result.x = maxX-DISTANCE_FROM_EDGE
    end
    return result
end

-- @description return processed corridor paths data as a table of arrays of points
return function(start, target)
    local maxX = Game.mapSizeX
    local maxZ = Game.mapSizeZ
    local path = {}
    -- get nearest vertical edge
    local firstPoint = GetVerticalEdge(start, maxX, maxZ)
    path[#path+1] = firstPoint
    -- get opposite horizontal edge
    local x = firstPoint.x
    local z = firstPoint.z > maxZ / 2 and DISTANCE_FROM_EDGE or maxZ - DISTANCE_FROM_EDGE
    path[#path+1] = Vec3(x, GetGroundHeight(x, z), z)
    -- get opposite vertical edge
    x = maxX - x
    path[#path+1] = Vec3(x, GetGroundHeight(x, z), z)

    return path
end