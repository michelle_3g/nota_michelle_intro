local sensorInfo = {
	name = "GridOfSafePointsDebug",
	desc = "Displays a grid of points",
	author = "Michelle",
	date = "2022-06-13",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description display a grid of points
return function(safePoints)
    if (Script.LuaUI('exampleDebug_update')) then
        local ID = 1
        for x, value in pairs(safePoints) do
            for z, point in pairs(value) do
                Script.LuaUI.exampleDebug_update(
                    ID, -- key
                    {	-- data - starting and ending point of the line
                        startPos = point.position - Vec3(30,0,0), 
                        endPos = point.position + Vec3(30,0,0), 
                    }
                )
                ID = ID + 1
                Script.LuaUI.exampleDebug_update(
                    ID, -- key
                    {	-- data - starting and ending point of the line
                        startPos = point.position - Vec3(0,0,30), 
                        endPos = point.position + Vec3(0,0,30), 
                    }
                )
                ID = ID + 1
            end
        end
        
    end
    return nil
end