local sensorInfo = {
	name = "ExtractCorridorPaths",
	desc = "Returns processed corridor paths data as a table of arrays of points.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return processed corridor paths data as a table of arrays of points
return function(corridors)
    local result = {}
    for corridorName, corridorDesc in pairs(corridors) do
        result[corridorName] = {}
        for i, point in ipairs(corridorDesc.points) do
            result[corridorName][i] = point.position
        end
    end
    return result
end