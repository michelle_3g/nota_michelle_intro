local sensorInfo = {
	name = "AllInfiltratorsReady",
	desc = "Returns true if all the Infiltrators are ready to move on their positions.",
	author = "Michelle",
	date = "2022-06-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- @description returns true if all the Infiltrators are ready to move on their positions
return function()
    if #bb.units.infiltrators == 0 then return false end
    for _, spyID in ipairs(bb.units.infiltrators) do
        local status = bb.units.status[spyID]
        if status ~= "ready" then
            return false
        end
    end
    return true
end