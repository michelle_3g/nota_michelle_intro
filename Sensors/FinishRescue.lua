local sensorInfo = {
	name = "FinishRescue",
	desc = "Changes status of the given triple (Atlas, unitToRescue, destinationPoint) according to the result of the rescue mission.",
	author = "Michelle",
	date = "2022-06-13",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GetUnitTransporter = Spring.GetUnitTransporter

-- @description changes status of the given triple (Atlas, unitToRescue, destinationPoint) according to the result of the rescue mission
return function(rescueInfo, success)
    -- change status of the atlas
    -- if it is dead --> dead
    local unitIsDead = UnitIsDead(rescueInfo.atlas)
    if unitIsDead == true or unitIsDead == nil then
        bb.rescueManager.atlasStatus[rescueInfo.atlas] = "dead"
    -- if it is alive and not occupied --> available
    elseif #GetUnitIsTransporting(rescueInfo.atlas) == 0 then
        bb.rescueManager.atlasStatus[rescueInfo.atlas] = "available"
    end

    -- change status of the unit to rescue and the destination position
    -- if it is dead --> dead
    unitIsDead = UnitIsDead(rescueInfo.unitToRescue)
    if unitIsDead == true or unitIsDead == nil then
        bb.rescueManager.unitToRescueStatus[rescueInfo.unitToRescue] = "dead"
        bb.rescueManager.positionStatus[rescueInfo.destinationIndex] = "free"
    else
        -- if it is in the safe area --> safe
        if GetDistance(bb.rescueManager.safeArea.center, Vec3(GetUnitPosition(rescueInfo.unitToRescue))) < bb.rescueManager.safeArea.radius then
            bb.rescueManager.unitToRescueStatus[rescueInfo.unitToRescue] = "safe"
            bb.rescueManager.positionStatus[rescueInfo.destinationIndex] = "occupied"
        -- if it is not being transported --> on_battlefield
        elseif GetUnitTransporter(rescueInfo.unitToRescue) == nil then
            bb.rescueManager.unitToRescueStatus[rescueInfo.unitToRescue] = "on_battlefield"
            bb.rescueManager.positionStatus[rescueInfo.destinationIndex] = "free"
        end
        -- if the rescue mission was not successful but the unit survived, the unit is probably in a dangerous area --> mark it as in danger
        if not success then
            bb.rescueManager.unitToRescueStatus[rescueInfo.unitToRescue] = "in_danger"
        end
    end



end