local sensorInfo = {
	name = "UpdateRescueStatus",
	desc = "Changes status of the units in the rescue manager if necessary.",
	author = "Michelle",
	date = "2022-06-13",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetUnitPosition = Spring.GetUnitPosition
local UnitIsDead = Spring.GetUnitIsDead
local GetUnitsInRectangle = Spring.GetUnitsInRectangle
local GetMyTeamID = Spring.GetMyTeamID

function UnitIsSafe(safeArea, unitID)
    local diff = safeArea.center - Vec3(GetUnitPosition(unitID))
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
    return dist < safeArea.radius
end

-- @description change status of the units in the rescue manager if necessary
return function(atlases, unitsToRescue)

    -- change status of the atlases
    for _, atlasID in ipairs(atlases) do
        -- if it is dead --> dead
        local unitIsDead = UnitIsDead(atlasID)
        if unitIsDead == true or unitIsDead == nil then
            bb.rescueManager.atlasStatus[atlasID] = "dead"
        end
    end
    
    -- change status of the units to rescue
    for _, unitID in ipairs(unitsToRescue) do
        -- if it is dead --> dead
        local unitIsDead = UnitIsDead(unitID)
        if unitIsDead == true or unitIsDead == nil then
            bb.rescueManager.unitToRescueStatus[unitID] = "dead"
        -- if it is in the safe area --> safe
        elseif UnitIsSafe(bb.rescueManager.safeArea, unitID) then
            bb.rescueManager.unitToRescueStatus[unitID] = "safe"
        -- if it is marked as safe but not in the safe area --> on_battlefield
        elseif bb.rescueManager.unitToRescueStatus[unitID] == "safe" and not UnitIsSafe(bb.rescueManager.safeArea, unitID) then
            bb.rescueManager.unitToRescueStatus[unitID] = "on_battlefield"
        end
    end

    -- change status of the safe positions
    local halfSize = bb.rescueManager.positionOffset/2
    for index=1, #bb.rescueManager.safePositions do
        local positionStatus = bb.rescueManager.positionStatus[index]
        local position = bb.rescueManager.safePositions[index]
        local unitsNearby = #GetUnitsInRectangle(position.x-halfSize, position.z-halfSize, position.x+halfSize, position.z+halfSize, GetMyTeamID())
        -- if free and unit nearby --> occupied
        if positionStatus == "free" and unitsNearby > 0 then bb.rescueManager.positionStatus[index] = "occupied" end
        -- if occupied and no unit nearby --> free
        if positionStatus == "occupied" and unitsNearby == 0 then bb.rescueManager.positionStatus[index] = "free" end
    end

end