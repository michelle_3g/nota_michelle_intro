# README #

This repository contains my solutions for the HLAA course so far.

## Behaviours

- `sandsail` - battle commander moves in the direction of wind, other units follow him in a grid formation
- `ctp2` - a bonus version which assigns Peewees to the closest unarmored platforms to capture them and then loads Warriors onto the Bear and transports them to the closest armored platform
- `ttdr` - a bonus version which rescues the units on the map using safe paths computed after scanning the map with Peepers
  - `ttdr-rescueUnit` - a sequence of commands to use one Atlas to save one unit
  - `ttdr-rescueUnitUI` - an order tree version of `ttdr-rescueUnit`
  - `ttdr-rescueUnits` - a parallel utilization of four Atlases
  - `ttdr-scanMapForSafePaths` - sends Peepers over the map, collects information about enemies, finds a grid of safe points and computes safe paths to all the units
- `swampdota` - buys basic units (Mavericks, Infiltrators and Lugers), transports them using Atlases behind the enemy base and then sends them to destroy the middle lane
  - `swampdotaInit` - initialization of variables
  - `shoppingManager` - manages purchases of units, buys 5 Atlases, 4 Infiltrators and then tries to balance Mavericks and Lugers
  - `transportUnits` - similarly to `ttdr-rescueUnits` utilizes 5 Atlases in parallel to transport units
    - `transportUnit` - a sequence of commands to transport a single unit by a single Atlas
  - `deployInfiltrators` - deploys Infiltrators to reveal important areas before attack (last N points on the middle lane)
  - `captureEnemyBase` - when there are enough units to attack, send them along the middle lane to destroy everything in their path

## Commands

- \[ctp2\] `moveUnit` - repeatedly issues a command to the given unit to move to the given position, succeeds if the unit is within the given tolerance away from the target position
- \[ctp2\] `moveUnitsIndependently` - repeatedly issues commands to the given units to move to the given positions (with a corresponding index), succeeds if all the units are within the given tolerance away from the target position
- \[ctp2\] `loadUnits` - loads the given units to the given transporter, succeeds if all the units are loaded (but it may not be on the same transporter)
- \[ctp2\] `unloadUnits` - unloads the given units from the given trasporter, succeeds if all the units are not loaded on the transporter
- \[ctp2\] `waitUntilLoadedOrUnloaded` - waits until all the given units are loaded/unloaded (depending on a bool parameter) to some transporter
- \[ttdr\] `loadUnit` - loads the given unit to the given transporter
- \[ttdr\] `unloadUnit` - unloads the given unit from the given transporter to the given position
- \[ttdr\] `unloadAllTransporters` - unloads all the given transporters
- \[ttdr\] `scanMapForEnemies` - scans the whole map using the given array of Peepers, stores information about the enemies found
- \[ttdr\] `findGridOfSafePoints` - scans the whole map as a grid of points and stores the safe ones (with respect to the enemies info) in a two-dimensional table
- \[ttdr\] `computePathsOverSafeGrid` - finds paths on a grid of safe points from the center of the safe area to anywhere else
- \[ttdr\] `extractPathsToUnits` - for each unit it finds a path as an array of points on a map from the points on a safe grid and precomputed paths (from the unit to the safe area)
- \[ttdr\] `followPath` - moves the given unit along the given path (in the chosen direction), each point is reached if the unit is within the given tolerance
- \[swampdota\] `buyUnit` - buys a unit woth the given name
- \[swampdota\] `upgradeLine` - upgrades a line with the given name by the given number of levels

## Sensors

- \[sandsail\] `ClosestOtherUnit` - returns distance between the battle commander and the closest other unit
- \[sandsail\] `FormationInfo` - returns grid formation data (units covering points on a grid behind the commander), e.g. positions, group definition, whether a commander is assigned
- \[ctp2\] `GetAveragePositionAndRadius` - returns average position of the given units and radius of the circle containing them
- \[ctp2\] `GetPlatforms` - returns description (center, width and height) of platforms higher than the given threshold, sorted by distance from the given starting point, divided into two groups (armored, unarmored, according to the given enemy positions)
- \[ctp2\] `GetPlatformsCoordinates` - returns an array of platforms' centers obtained from the given platforms' description
- \[ttdr\] `FilterUnitsForRescueMission` - returns a table of units filtered by category into Atlases, Peepers and others
- \[ttdr\] `InitializeRescueManager` - initializes a reservation system for rescue of the given units
- \[ttdr\] `ArrangeNewRescue` - returns a triple of an available Atlas, the unit to rescue which is the closest to the safe area and a safe destination point (while changing their status in the rescue manager accordingly)
- \[ttdr\] `FinishRescue` - changes status of the given triple (Atlas, unitToRescue, destinationPoint) according to the result of the rescue mission
- \[ttdr\] `UpdateRescueStatus` - changes status of the units in the rescue manager if necessary (should be called periodically to automatically adjust status)
- \[ttdr\] `AllAtlasesDead` - returns true if all Atlases are dead, false otherwise
- \[ttdr\] `AllUnitsRescuedOrDead` - returns true if all the units are either safe or dead (based on their status in the rescue manager), false otherwise
- \[swampdota\] `AllInfiltratorsReady` - returns true if all the Infiltrators are ready to move on their positions (have a status "ready")
- \[swampdota\] `ArrangeNewTransport` - returns a pair of an available Atlas and a unit waiting to be transported
- \[swampdota\] `ExtractCorridorPaths` - returns processed corridor paths data as a table of arrays of points
- \[swampdota\] `FindPointBehindEnemyBase` - returns a point which is far behind an enemy base
- \[swampdota\] `FindSafePathAlongMapEdge` - returns an array of points going from the given start to the given target while following a map edge
- \[swampdota\] `GetPositionsFromFormation` - returns an array (of length N) of absolute positions derived from the relative formation and the destination
- \[swampdota\] `GetReadyUnits` - returns an array of units ready for combat
- \[swampdota\] `GetSpyPositions` - returns an array of positions for infiltrator deployment (last N positions on the given path)
- \[swampdota\] `HaveEnoughMetal` - returns true if we have enough metal to purchase the given item (unit / line upgrade), false otherwise
- \[swampdota\] `TransporterReturned` - changes status of the given Atlas to "available"
- \[swampdota\] `TransportFailed` - changes status of the given pair (Atlas, unitToTransport) according to the cause of failure
- \[swampdota\] `UnitTransported` - changes status of the given pair (Atlas, unitToTransport) after unloading the unit
- \[swampdota\] `UpdateUnitCounts` - updates counts of available units divided into categories
- \[swampdota\] `UpdateUnitsStatusForSwampdota` - updates status of units filtered by category into Atlases, Mavericks, Lugers, Infiltrators and others

### Debug

- \[sandsail\] `WindDebug` - returns starting and ending point of a wind debug line
- \[ttdr\] `GridOfSafePointsDebug` - displays a grid of safe points on the map
- \[ttdr\] `PathsToUnitsDebug` - displays the given paths for each unit
- \[swampdota\] `SwampdotaPathDebug` - displays the given path which is in the format from swampdota

## UnitCategories

- \[sandsail\] `battleCommanderCategory` - contains only `armbcom` (Battle Commander)
- \[ctp2\] `peeweeCategory` - contains only `armpw` (Peewee)
- \[ctp2\] `warriorCategory` - contains only `armwar` (Warrior)
- \[ctp2\] `bearCategory` - contains only `armthovr` (Bear)
- \[ttdr\] `atlasCategory` - contains only `armatlas` (Atlas)
- \[ttdr\] `peeperCategory` - contains only `armpeep` (Peeper)
- \[swampdota\] `boxCategory` - 
- \[swampdota\] `dragonflyCategory` - 
- \[swampdota\] `farckCategory` - 
- \[swampdota\] `infiltratorCategory` - 
- \[swampdota\] `lugerCategory` - 
- \[swampdota\] `maverickCategory` - 
- \[swampdota\] `seerCategory` - 
- \[swampdota\] `zeusCategory` - 

