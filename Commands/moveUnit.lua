function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move the given unit to the given position.",
		parameterDefs = {
			{ 
				name = "assigned_unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter assigned_unit - unitID of the unit to move
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter position - Vec3 of position to move to
			{ 
				name = "tolerance",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "5",
			},
			-- @parameter tolerance - number
		}
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- speed-ups
local UnitIsDead = Spring.GetUnitIsDead
local GetUnitPosition = Spring.GetUnitPosition
local GiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
    local unitID = parameter.assigned_unit  -- unitID
    local position = parameter.position  -- Vec3
    local tolerance = parameter.tolerance  -- number

    -- initialization
    if not self.is_initialized then
        -- validation
        if not Spring.ValidUnitID(unitID) then
            Logger.warn("nota_michelle_intro.moveUnit", "The unitID [" .. unitID .. "] is not valid.") 
            return FAILURE
        end
        -- the position should be on map (or the closest one on map should be picked)
		if position.x < 0 then position.x = 0 end
		if position.x > Game.mapSizeX then position.x = Game.mapSizeX end
		if position.z < 0 then position.z = 0 end
		if position.z > Game.mapSizeZ then position.z = Game.mapSizeZ end

		-- initialization
		self.last_position = Vec3(GetUnitPosition(unitID))
		self.frames_without_movement = 0
        self.is_initialized = true
    end

-- check all success conditions
	-- unit is close to the target
    local unit_position = Vec3(GetUnitPosition(unitID))
	local diff = position - unit_position
	local dist = math.sqrt(diff.x * diff.x + diff.z * diff.z)
	if dist < tolerance then return SUCCESS end

-- check all failure conditions
	-- unit is dead
	unitIsDead = UnitIsDead(unitID)
    if unitIsDead == true or unitIsDead == nil then
        Logger.warn("nota_michelle_intro.moveUnit", "The unit [" .. unitID .. "] is dead.") 
        return FAILURE
    end
	-- unit has not moved in a very long time
	local unitPosition = Vec3(GetUnitPosition(unitID))
	local dist = GetDistance(unitPosition, self.last_position)
	if dist < 0.01 then
		self.frames_without_movement = self.frames_without_movement + 1
		if self.frames_without_movement > 50 then
			Logger.warn("nota_michelle_intro.moveUnit", "The unit [" .. unitID .. "] is stuck.")
			return FAILURE
		end
	else
		self.last_position = unitPosition
		self.frames_without_movement = 0
	end

-- issue move command
    GiveOrderToUnit(unitID, CMD.MOVE, position:AsSpringVector() , {})

-- otherwise running
    return RUNNING
end

function Reset(self)
    self.is_initialized = false
end