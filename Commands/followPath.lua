function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move the given unit along the given path (in the chosen direction). Each point is reached if the unit is within the given tolerance.",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter unitID - unitID of the unit to move
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter path [array] - an array of points
			{ 
				name = "reversed",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
			-- @parameter reversed [bool] - direction of the path
			{ 
				name = "tolerance",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "5",
			},
			-- @parameter tolerance [number] - how close to each point the unit should be
		}
	}
end

function GetDistance(position1, position2)
	local diff = position1 - position2
	local dist = math.sqrt(diff.x*diff.x + diff.z*diff.z)
	return dist
end

-- speed-ups
local GiveOrderToUnit = Spring.GiveOrderToUnit
local GetUnitPosition = Spring.GetUnitPosition
local UnitIsDead = Spring.GetUnitIsDead

function Run(self, units, parameter)
    local unitID = parameter.unitID  -- unitID
    local path = parameter.path  -- array
    local reversed = parameter.reversed  -- bool
    local tolerance = parameter.tolerance  -- number

    -- initialization
    if not self.is_initialized then
        -- validation
        if not Spring.ValidUnitID(unitID) then
            Logger.warn("nota_michelle_intro.followPath", "The unitID [" .. unitID .. "] is not valid.") 
            return FAILURE
        end
        
        if path == nil then
            Logger.warn("nota_michelle_intro.followPath", "The unitID [" .. unitID .. "] on position [" .. GetUnitPosition(unitID) .. "] does not have a path assigned.")
            return FAILURE
        end
        
        -- initialization
        self.i = 1
        if reversed then
            self.i = #path
        end
        self.last_position = Vec3(GetUnitPosition(unitID))
		self.frames_without_movement = 0

        -- issue move commands
        while self.i > 0 and self.i <= #path do
            GiveOrderToUnit(unitID, CMD.MOVE, path[self.i]:AsSpringVector() , {"shift"})
            if not reversed then self.i = self.i + 1 end
            if reversed then self.i = self.i - 1 end
        end

        self.is_initialized = true
    end

-- check all failure conditions
    -- unit is dead
    unitIsDead = UnitIsDead(unitID)
    if unitIsDead == true or unitIsDead == nil then
        Logger.warn("nota_michelle_intro.followPath", "The unit [" .. unitID .. "] is dead.") 
        return FAILURE
    end
    -- unit has not moved in a very long time
	local unitPosition = Vec3(GetUnitPosition(unitID))
	local dist = GetDistance(unitPosition, self.last_position)
	if dist < 0.01 then
		self.frames_without_movement = self.frames_without_movement + 1
		if self.frames_without_movement > 50 then
            Logger.warn("nota_michelle_intro.followPath", "The unit [" .. unitID .. "] is stuck.")
            return FAILURE
        end
	else
		self.last_position = unitPosition
		self.frames_without_movement = 0
	end

-- check all success conditions
    -- unit is at the last point
    local unit_position = Vec3(GetUnitPosition(unitID))
    local target_position = nil
    if reversed then target_position = path[1]
    else target_position = path[#path] end
	local diff = target_position - unit_position
	local dist = math.sqrt(diff.x * diff.x + diff.z * diff.z)
    if dist < tolerance then
        return SUCCESS
    end

-- otherwise running
    return RUNNING
end

function Reset(self)
    self.is_initialized = false
    self.i = 1
end