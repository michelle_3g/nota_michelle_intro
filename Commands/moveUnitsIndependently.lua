function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move the given units to the given positions independently, not as a group.",
		parameterDefs = {
			{ 
				name = "assigned_units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter assigned_units [array] - list of unitID
			--[[ local example = {
				[1] = 14945,
				[2] = 5814,
				[3] = 126450,
			}
			]]--
			{ 
				name = "positions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter positions [array] - list of Vec3
			--[[ local example = {
				[1] = Vec3(0,0,0),
				[2] = Vec3(10,0,0),
				[3] = Vec3(-10,30,0),
			}
			]]--
			{ 
				name = "tolerance",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "5",
			},
			-- @parameter tolerance - number
		}
	}
end

-- speed-ups
local GiveOrderToUnit = Spring.GiveOrderToUnit
local IsValidUnitID = Spring.ValidUnitID
local GetUnitPosition = Spring.GetUnitPosition

function Run(self, units, parameter)
    local assigned_units = parameter.assigned_units  -- array of unitID
    local positions = parameter.positions  -- array of Vec3
    local tolerance = parameter.tolerance  -- number

    -- initialization
    if not self.is_initialized then
        -- validation
		--    there are enough positions for all the units
        if #positions < #assigned_units then
            Logger.warn("nota_michelle_intro.moveUnitsIndependently", "Number of positions [" .. #positions .. "] is smaller than needed for given count of units [" .. #assigned_units .. "].") 
            return FAILURE
        end
        for _, unitID in ipairs(assigned_units) do
			--    unit is valid
            if not IsValidUnitID(unitID) then
                Logger.warn("nota_michelle_intro.moveUnitsIndependently", "The unitID [" .. unitID .. "] is not valid.") 
                return FAILURE
            end
        end
        -- TODO: the position should be on map (or the closest one on map shoulp be picked)
        self.is_initialized = true
    end

    -- check all success conditions
    local is_success = true
    for i = 1, #assigned_units do
        -- check whether the unit is on the position (within tolerance)
        local unit_position = Vec3(GetUnitPosition(assigned_units[i]))
        if positions[i]:Distance(unit_position) > tolerance then is_success = false end
    end
    if is_success then return SUCCESS end

    -- check all failure conditions

    -- otherwise running

    -- issue move command
    for i = 1, #assigned_units do
        GiveOrderToUnit(assigned_units[i], CMD.MOVE, positions[i]:AsSpringVector() , {})
    end

    return RUNNING
end

function Reset(self)
    self.is_initialized = false
end