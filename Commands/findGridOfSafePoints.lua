function getInfo()
	return {
		tooltip = "Scan the whole map as a grid of points and store the safe ones in a two-dimensional table.",
		parameterDefs = {
			{ 
				name = "enemyUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter enemyUnits [table] - a table of information about enemies
			--[[ local example = {
				[14945] = { [position] = Vec3(0, 0, 0), [unitDef] = {...} },
				[5814] = { [position] = Vec3(0, 0, 0), [unitDef] = {...} },
				[126450] = { [position] = Vec3(0, 0, 0), [unitDef] = {...} },
			}
			]]--
			{ 
				name = "step",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter step [number] - offset between points
			{ 
				name = "heightThreshold",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter heightThreshold [number] - height threshold determining safety of a point
			{ 
				name = "enemyDistanceThreshold",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter enemyDistanceThreshold [number] - at least how far should be the enemies to consider the point safe
			{ 
				name = "enemyHeightThreshold",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter enemyHeightThreshold [number] - at least how far above should be the enemies to consider the point safe
			{ 
				name = "lowEnemyDistanceThreshold",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter lowEnemyDistanceThreshold [number] - at least how far should be the enemies to consider the point safe if it is below heightThreshold
		}
	}
end

-- speed-ups
local GetGroundHeight = Spring.GetGroundHeight

function GetNearbyEnemies(position, enemyUnits, enemyDistanceThreshold, enemyHeightThreshold)
    local nearbyEnemies = {
        count = 0,
        above = 0,
        closest = math.huge
    }

    for _, enemyInfo in pairs(enemyUnits) do
        -- TODO: Check attack range of the units instead of the distance
        local dist = enemyInfo.position:Distance(position)
        if dist < nearbyEnemies.closest then
            nearbyEnemies.closest = dist
        end
        if dist < enemyDistanceThreshold then
            nearbyEnemies.count = nearbyEnemies.count + 1
            if position.y + enemyHeightThreshold < enemyInfo.position.y then
                nearbyEnemies.above = nearbyEnemies.above + 1
            end
        end
    end

    return nearbyEnemies
end

function AddSafePoint(position)
    local s = bb.safePointsStep
    local cell = Vec3(math.floor(position.x/s)+1, 0, math.floor(position.z/s)+1)
    if bb.safePoints[cell.x] == nil then
        bb.safePoints[cell.x] = {}
    end
    if bb.safePoints[cell.x][cell.z] == nil then
        bb.safePoints[cell.x][cell.z] = {}
    end
    bb.safePoints[cell.x][cell.z].position = position
end

function Run(self, units, parameter)
    local enemyUnits = parameter.enemyUnits
    local step = parameter.step
    local heightThreshold = parameter.heightThreshold
    local enemyDistanceThreshold = parameter.enemyDistanceThreshold
    local enemyHeightThreshold = parameter.enemyHeightThreshold
    local lowEnemyDistanceThreshold = parameter.lowEnemyDistanceThreshold

    local maxX = Game.mapSizeX
    local maxZ = Game.mapSizeZ

    -- initialization
    if not self.is_initialized then
        bb.safePoints = {}
        bb.safePointsStep = step
        self.currentZ = 1
        self.is_initialized = true
    end

    -- go through five lines of points in one frame
    for i=1, 5 do
        for x=1, maxX, step do
            local position = Vec3(x, GetGroundHeight(x, self.currentZ), self.currentZ)
            -- go through all the enemy units
            local nearbyEnemies = GetNearbyEnemies(position, enemyUnits, enemyDistanceThreshold, enemyHeightThreshold)
            -- if there are no enemy units nearby, the point is safe
            if nearbyEnemies.count == 0 then
                AddSafePoint(position)
            -- if there are enemies nearby, but they are above the point, the point is safe
            elseif nearbyEnemies.count == nearbyEnemies.above then
                AddSafePoint(position)
            -- if there are enemies nearby, but the point is low enough and the enmeies are not really close, then it is quite safe
            elseif position.y < heightThreshold and nearbyEnemies.closest > lowEnemyDistanceThreshold then
                AddSafePoint(position)
            end
            -- TODO: if the point is not in a line of sight, then it is safe
        end
        self.currentZ = self.currentZ + step
        if self.currentZ > maxZ then
            break
        end
    end
    
    -- check all success conditions
    --    all points have been processed
    if self.currentZ > maxZ then
        return SUCCESS
    end

    -- otherwise running
    return RUNNING
end

function Reset(self)
    self.is_initialized = false
    self.currentZ = 1
end