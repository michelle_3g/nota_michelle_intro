function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Scan the whole map using the given array of Peepers, store information about the enemies found.",
		parameterDefs = {
			{ 
				name = "peepers",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter peepers [array] - list of unitID
			--[[ local example = {
				[1] = 14945,
				[2] = 5814,
				[3] = 126450,
			}
			]]--
		}
	}
end

-- speed-ups
local GiveOrderToUnit = Spring.GiveOrderToUnit
local IsValidUnitID = Spring.ValidUnitID
local UnitIsDead = Spring.GetUnitIsDead
local GetUnitPosition = Spring.GetUnitPosition
local GetGroundHeight = Spring.GetGroundHeight
local GetUnitDefID = Spring.GetUnitDefID

function Run(self, units, parameter)
    local peepers = parameter.peepers  -- array of unitID

    -- initialization
    if not self.is_initialized then
        -- validation
        for _, unitID in ipairs(peepers) do
            -- units are valid
            if not IsValidUnitID(unitID) then
                Logger.warn("nota_michelle_intro.scanMapForEnemies3", "The unitID [" .. unitID .. "] is not valid.") 
                return FAILURE
            end
            -- units are Peepers
            if UnitDefs[GetUnitDefID(unitID)].name ~= "armpeep" then
                Logger.warn("nota_michelle_intro.scanMapForEnemies3", "The unit with ID [" .. unitID .. "] is not Peeper.") 
                return FAILURE
            end
        end

        -- initialization
        self.progress = {}
        bb.enemiesInfo = {}
        -- issue move commands for each Peeper
        self.firstZ = Game.mapSizeZ
        self.secondZ = 100
        local offset = Game.mapSizeX / #peepers
        for i, unitID in ipairs(peepers) do
            self.progress[unitID] = 0
            local x = offset * i - offset / 2
            -- spread out at the top of the map
            GiveOrderToUnit(unitID, CMD.MOVE, Vec3(x, GetGroundHeight(x, self.secondZ-50), self.secondZ-50):AsSpringVector() , {})
            -- fly directly to the south
            GiveOrderToUnit(unitID, CMD.MOVE, Vec3(x, GetGroundHeight(x, self.firstZ), self.firstZ):AsSpringVector() , {"shift"})
            -- and back to the north
            GiveOrderToUnit(unitID, CMD.MOVE, Vec3(x, GetGroundHeight(x, self.secondZ), self.secondZ):AsSpringVector() , {"shift"})
        end

        self.is_initialized = true
    end

    -- store enemies information
    local enemies = Sensors.core.EnemyUnits()
    for index, enemyUnitID in ipairs(enemies) do
        if bb.enemiesInfo[enemyUnitID] == nil then
            bb.enemiesInfo[enemyUnitID] = {}
        end
        bb.enemiesInfo[enemyUnitID].position = Vec3(GetUnitPosition(enemyUnitID))  -- enemy position
        local unitDefID = GetUnitDefID(enemyUnitID)
        if unitDefID ~= nil then  -- type of the unit is known, so save it
            local unitDef = UnitDefs[unitDefID]
            if unitDef ~= nil then
                bb.enemiesInfo[enemyUnitID].unitDef = unitDef
            end
        end
    end

    -- update units' progress
    for i, unitID in ipairs(peepers) do
        local unitIsDead = UnitIsDead(unitID)
        if unitIsDead == true or unitIsDead == nil then
            self.progress[unitID] = -1  -- unit is dead
        else
            local unit_position = Vec3(GetUnitPosition(unitID))
            if unit_position.z > Game.mapSizeZ-256 then self.progress[unitID] = 1 end  -- unit reached the end of the map
        end
    end

-- check all success conditions
    -- all units are either dead or returned
    local isSuccess = true
    for i, unitID in ipairs(peepers) do
        local unitProgress = self.progress[unitID]
        if unitProgress ~= -1 and unitProgress ~= 1 then  -- not dead, not finished
            isSuccess = false
            break
        end
    end
    if isSuccess then return SUCCESS end

-- otherwise running
    return RUNNING
end

function Reset(self)
    self.is_initialized = false
    self.progress = {}
    self.firstZ = 0
    self.secondZ = 0
end