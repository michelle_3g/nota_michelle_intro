function getInfo()
	return {
		tooltip = "Find paths on a grid of safe points from the center of the safe area to anywhere else.",
		parameterDefs = {
			{ 
				name = "safeArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter safeArea [table] - a safe area defined by center and radius
            --[[ local example = {
				[center] = Vec3(1500, 800, 3300),
				[radius] = 600,
			}
			]]--
		}
	}
end

function GetClosestSafePoint(position)
    local s = bb.safePointsStep
    local cell = Vec3(math.floor(position.x/s)+1, 0, math.floor(position.z/s)+1)
    return cell
    -- TODO: if the position is not directly in a safe cell, find the closest one
end

function Run(self, units, parameter)
    local safeArea = parameter.safeArea

    -- initialization
    if not self.is_initialized then
        self.queue = {}
        local startCell = GetClosestSafePoint(safeArea.center)
        bb.safePoints[startCell.x][startCell.z].distance = 0
        bb.safePoints[startCell.x][startCell.z].next = nil
        bb.safePoints[startCell.x][startCell.z].maxHeight = safeArea.center.y
        self.queue[#self.queue+1] = startCell
        self.is_initialized = true
    end

    -- perform 100 iterations of BFS in one frame
    local i = 1
    while #self.queue > 0 and i <= 100 do
        -- remove the point with the lowest height
        local index = 1
        local minHeight = math.huge
        for key, cell2 in pairs(self.queue) do
            local safePoint = bb.safePoints[cell2.x][cell2.z]
            if safePoint.position.y < minHeight then
                minHeight = safePoint.position.y
                index = key
            end
        end
        local cell = table.remove(self.queue, index)
        -- go through the neighbours
        for x=-1, 1 do
            for z=-1, 1 do
                if not (x ~= 0 and z ~= 0) then
                    if bb.safePoints[cell.x+x] ~= nil and bb.safePoints[cell.x+x][cell.z+z] ~= nil then -- safe point
                        if bb.safePoints[cell.x+x][cell.z+z].distance == nil then -- not visited
                            -- update info and add to queue
                            bb.safePoints[cell.x+x][cell.z+z].distance = bb.safePoints[cell.x][cell.z].distance + 1
                            bb.safePoints[cell.x+x][cell.z+z].next = cell
                            bb.safePoints[cell.x+x][cell.z+z].maxHeight = math.max(bb.safePoints[cell.x][cell.z].maxHeight, bb.safePoints[cell.x+x][cell.z+z].position.y)
                            table.insert(self.queue, Vec3(cell.x+x, 0, cell.z+z))
                        end
                    end
                end
            end
        end
        i = i + 1
    end
    
    -- check all success conditions
    --    all points have been processed
    if #self.queue == 0 then
        return SUCCESS
    end

    -- otherwise running
    return RUNNING
end

function Reset(self)
    self.is_initialized = false
    self.queue = {}
end