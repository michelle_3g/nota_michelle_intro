function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload the given unit from the given transporter to the given position.",
		parameterDefs = {
			{ 
				name = "unit_to_unload",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter unit_to_unload - unitID
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter transporter - unitID
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter position - Vec3
		}
	}
end

-- speed-ups
local GiveOrderToUnit = Spring.GiveOrderToUnit
local UnitIsDead = Spring.GetUnitIsDead
local ValidUnitID = Spring.ValidUnitID
local UnitDefID = Spring.GetUnitDefID
local GetUnitTransporter = Spring.GetUnitTransporter
local UnitPosition = Spring.GetUnitPosition

function Run(self, units, parameter)
    local unit_to_unload = parameter.unit_to_unload  -- unitID
    local transporter = parameter.transporter  -- unitID
    local position = parameter.position  -- Vec3

-- initialization
    if not self.is_initialized then
    -- validation
        -- transporter is valid
        if not ValidUnitID(transporter) then
            Logger.warn("nota_michelle_intro.unloadUnit", "The transporter ID [" .. transporter .. "] is not valid.") 
            return FAILURE
        end
        -- transporter is actually a transporter
        local unitDefID = UnitDefID(transporter)
        if not UnitDefs[unitDefID].isTransport then
            Logger.warn("nota_michelle_intro.unloadUnit", "The unit with ID [" .. transporter .. "] is not transport.") 
            return FAILURE
        end
        -- unit_to_unload is valid
        if not ValidUnitID(unit_to_unload) then
            Logger.warn("nota_michelle_intro.unloadUnit", "The unit_to_unload ID [" .. unit_to_unload .. "] is not valid.") 
            return FAILURE
        end
        -- unit_to_unload is loaded on the transporter
        if GetUnitTransporter(unit_to_unload) ~= transporter then
            return FAILURE
        end

        -- issue unload command
        GiveOrderToUnit(transporter, CMD.UNLOAD_UNIT, {position.x, position.y, position.z}, {})
        
        self.is_initialized = true
    end


-- check all success conditions
    -- unit_to_load is not loaded onn transporter
    if GetUnitTransporter(unit_to_unload) == nil then
        return SUCCESS
    end

-- check all failure conditions
    -- transporter is dead
    local unitIsDead = UnitIsDead(transporter)
    if unitIsDead == true or unitIsDead == nil then
        Logger.warn("nota_michelle_intro.unloadUnit", "The transporter [" .. transporter .. "] is dead.") 
        return FAILURE
    end
    -- unit_to_load is dead
    unitIsDead = UnitIsDead(unit_to_unload)
    if unitIsDead == true or unitIsDead == nil then
        Logger.warn("nota_michelle_intro.unloadUnit", "The unit_to_unload [" .. unit_to_unload .. "] is dead.") 
        return FAILURE
    end
    -- TODO: CMD.UNLOAD_UNITS is not in the command queue (command queue is empty) - not working, returns FAILURE
    --if Spring.GetUnitCommands(transporter, 0) == 0 then
    --    Logger.warn("nota_michelle_intro.unloadUnit", "There is no command in the command queue.") 
    --    return FAILURE
    --end


-- otherwise running
    return RUNNING

end

function Reset(self)
    self.is_initialized = false
end