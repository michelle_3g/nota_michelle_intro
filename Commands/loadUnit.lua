function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load the given unit to the given transporter.",
		parameterDefs = {
			{ 
				name = "unit_to_load",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter unit_to_load - unitID
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter transporter - unitID
		}
	}
end

-- speed-ups
local GiveOrderToUnit = Spring.GiveOrderToUnit
local UnitIsDead = Spring.GetUnitIsDead
local ValidUnitID = Spring.ValidUnitID
local UnitDefID = Spring.GetUnitDefID
local GetUnitTransporter = Spring.GetUnitTransporter
local GetUnitIsTransporting = Spring.GetUnitIsTransporting

function Run(self, units, parameter)
    local unit_to_load = parameter.unit_to_load  -- unitID
    local transporter = parameter.transporter  -- unitID

-- initialization
    if not self.is_initialized then
    -- validation
        -- transporter is valid
        if not ValidUnitID(transporter) then
            Logger.warn("nota_michelle_intro.loadUnit", "The transporter ID [" .. transporter .. "] is not valid.") 
            return FAILURE
        end
        -- transporter is actually a transporter
        local unitDefID = UnitDefID(transporter)
        if not UnitDefs[unitDefID].isTransport then
            Logger.warn("nota_michelle_intro.loadUnit", "The unit with ID [" .. transporter .. "] is not transport.") 
            return FAILURE
        end
        -- unit_to_load is valid
        if not ValidUnitID(unit_to_load) then
            Logger.warn("nota_michelle_intro.loadUnit", "The unit_to_load ID [" .. unit_to_load .. "] is not valid.") 
            return FAILURE
        end
        -- unit_to_load can be transported
        unitDefID = UnitDefID(unit_to_load)
        if UnitDefs[unitDefID].cantBeTransported then
            Logger.warn("nota_michelle_intro.loadUnit", "The unit with ID [" .. unit_to_load .. "] cannot be transported.") 
            return FAILURE
        end

        -- issue load command
        GiveOrderToUnit(transporter, CMD.LOAD_UNITS, {unit_to_load}, {})
        
        self.is_initialized = true
    end


-- check all success conditions
    -- unit_to_load is loaded in transporter
    if GetUnitTransporter(unit_to_load) == transporter then
        return SUCCESS
    end

-- check all failure conditions
    -- transporter is dead
    local unitIsDead = UnitIsDead(transporter)
    if unitIsDead == true or unitIsDead == nil then
        Logger.warn("nota_michelle_intro.loadUnit", "The transporter [" .. transporter .. "] is dead.") 
        return FAILURE
    end
    -- unit_to_load is dead
    unitIsDead = UnitIsDead(unit_to_load)
    if unitIsDead == true or unitIsDead == nil then
        Logger.warn("nota_michelle_intro.loadUnit", "The unit_to_load [" .. unit_to_load .. "] is dead.") 
        return FAILURE
    end
    -- transporter is occupied already
    if #GetUnitIsTransporting(transporter) > 0 then
        Logger.warn("nota_michelle_intro.loadUnit", "The transporter is already occupied.") 
        return FAILURE
    end
    -- TODO: transporter is not capable of transporting unit_to_load (power)
    -- unit_to_load is transported already
    local tr = GetUnitTransporter(unit_to_load)
    if tr ~= nil and tr ~= transporter then
        Logger.warn("nota_michelle_intro.loadUnit", "The unit is already being transported.") 
        return FAILURE
    end
    -- TODO: CMD.LOAD_UNITS is not in the command queue (command queue is empty) - not working, returns FAILURE
    --if Spring.GetUnitCommands(transporter, 0) == 0 then
    --    Logger.warn("nota_michelle_intro.loadUnit", "There is no command in the command queue.") 
    --    return FAILURE
    --end


-- otherwise running
    return RUNNING

end

function Reset(self)
    self.is_initialized = false
end