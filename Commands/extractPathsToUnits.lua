function getInfo()
	return {
		tooltip = "For each unit extract an array of points on a map from the points on a safe grid and precomputed paths.",
		parameterDefs = {
			{ 
				name = "unitsToRescue",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter unitsToRescue [array] - an array of units to be rescued
			--[[ local example = {
				[0] = 14945,
				[1] = 5814,
				[2] = 126450,
			}
			]]--
		}
	}
end

-- speed-ups
local GetUnitPosition = Spring.GetUnitPosition

function GetClosestSafePoint(position)
    local s = bb.safePointsStep
    -- try the cell directly below
    local cell = Vec3(math.floor(position.x/s)+1, 0, math.floor(position.z/s)+1)
    if bb.safePoints[cell.x] ~= nil and bb.safePoints[cell.x][cell.z] ~= nil then
        return cell
    end
    -- look in the cells in 8 directions, at most 10 far
    local i = 1
    while i <= 10 do
        if bb.safePoints[cell.x+i] ~= nil and bb.safePoints[cell.x+i][cell.z] ~= nil then
            return Vec3(cell.x+i, 0, cell.z)
        end
        if bb.safePoints[cell.x-i] ~= nil and bb.safePoints[cell.x-i][cell.z] ~= nil then
            return Vec3(cell.x-i, 0, cell.z)
        end
        if bb.safePoints[cell.x] ~= nil and bb.safePoints[cell.x][cell.z+i] ~= nil then
            return Vec3(cell.x, 0, cell.z+i)
        end
        if bb.safePoints[cell.x] ~= nil and bb.safePoints[cell.x][cell.z-i] ~= nil then
            return Vec3(cell.x, 0, cell.z-i)
        end
        if bb.safePoints[cell.x+i] ~= nil and bb.safePoints[cell.x+i][cell.z+i] ~= nil then
            return Vec3(cell.x+i, 0, cell.z+i)
        end
        if bb.safePoints[cell.x+i] ~= nil and bb.safePoints[cell.x+i][cell.z-i] ~= nil then
            return Vec3(cell.x+i, 0, cell.z-i)
        end
        if bb.safePoints[cell.x-i] ~= nil and bb.safePoints[cell.x-i][cell.z+i] ~= nil then
            return Vec3(cell.x-i, 0, cell.z+i)
        end
        if bb.safePoints[cell.x-i] ~= nil and bb.safePoints[cell.x-i][cell.z-i] ~= nil then
            return Vec3(cell.x-i, 0, cell.z-i)
        end
        i = i + 1
    end
    -- TODO: find the closest one generally
    return cell
end

function Run(self, units, parameter)
    local unitsToRescue = parameter.unitsToRescue

    -- initialization
    if not self.is_initialized then
        self.i = 1
        bb.pathsToUnits = {}
        self.is_initialized = true
    end

    -- find paths to all the units
    while self.i <= #unitsToRescue do
        local unitID = unitsToRescue[self.i]
        -- get position of the unit
        local position = Vec3(GetUnitPosition(unitID))
        -- translate the position into a cell on the grid
        local cell = GetClosestSafePoint(position)
        -- follow the path and store the points along
        bb.pathsToUnits[unitID] = {}
        table.insert(bb.pathsToUnits[unitID], position)
        while bb.safePoints[cell.x][cell.z].next ~= nil do
            table.insert(bb.pathsToUnits[unitID], bb.safePoints[cell.x][cell.z].position)
            cell = bb.safePoints[cell.x][cell.z].next
        end
        self.i = self.i + 1
    end
    
    -- check all success conditions
    --    all units have been processed
    if self.i > #unitsToRescue then
        return SUCCESS
    end

    -- otherwise running
    return RUNNING
end

function Reset(self)
    self.is_initialized = false
    self.i = 1
end