function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload the given units from the given transporter.",
		parameterDefs = {
			{ 
				name = "assigned_units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter assigned_units [array] - list of unitID
			--[[ local example = {
				[1] = 14945,
				[2] = 5814,
				[3] = 126450,
			}
			]]--
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter transporter - unitID
		}
	}
end

-- speed-ups
local GiveOrderToUnit = Spring.GiveOrderToUnit
local IsValidUnitID = Spring.ValidUnitID
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitTransporter = Spring.GetUnitTransporter

function Run(self, units, parameter)
    local assigned_units = parameter.assigned_units  -- array of unitID
    local transporter = parameter.transporter  -- unitID

    -- initialization
    if not self.is_initialized then
        -- validation
        --   transporter is valid
        if not IsValidUnitID(transporter) then
            Logger.warn("nota_michelle_intro.unloadUnits", "The transporter ID [" .. transporter .. "] is not valid.") 
            return FAILURE
        end
        --    transporter is actually a transporter
        local unitDefID = GetUnitDefID(transporter)
        if UnitDefs[unitDefID].name ~= "armthovr" then  --  .isTransport was not working
            Logger.warn("nota_michelle_intro.unloadUnits", "The unit with ID [" .. transporter .. "] is not transport.") 
            return FAILURE
        end
        for _, unitID in ipairs(assigned_units) do
            --    unit is valid
            if not IsValidUnitID(unitID) then
                Logger.warn("nota_michelle_intro.unloadUnits", "The unit ID [" .. unitID .. "] is not valid.") 
                return FAILURE
            end
            --    unit can be transported
            unitDefID = GetUnitDefID(unitID)
            if UnitDefs[unitDefID].cantBeTransported then
                Logger.warn("nota_michelle_intro.unloadUnits", "The unit with ID [" .. unitID .. "] cannot be transported.") 
                return FAILURE
            end
        end
        -- issue unload commands
        local transporter_position = Vec3(GetUnitPosition(transporter))
        GiveOrderToUnit(transporter, CMD.UNLOAD_UNITS, {transporter_position.x, transporter_position.y, transporter_position.z, 50}, {"shift"})
        
        self.is_initialized = true
    end

    -- check all success conditions
    local is_success = true
    for _, unitID in ipairs(assigned_units) do
        if GetUnitTransporter(unitID) == transporter then
            is_success = false
            break
        end
    end
    if is_success then
        return SUCCESS
    end

    -- check all failure conditions

    -- otherwise running

    return RUNNING
end

function Reset(self)
    self.is_initialized = false
end