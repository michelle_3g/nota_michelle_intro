function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload all the given transporters.",
		parameterDefs = {
			{ 
				name = "transporters",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter transporters [array] - list of unitID
			--[[ local example = {
				[1] = 14945,
				[2] = 5814,
				[3] = 126450,
			}
			]]--
		}
	}
end

-- speed-ups
local GiveOrderToUnit = Spring.GiveOrderToUnit
local UnitIsDead = Spring.GetUnitIsDead
local IsValidUnitID = Spring.ValidUnitID
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitIsTransporting = Spring.GetUnitIsTransporting

function Run(self, units, parameter)
    local transporters = parameter.transporters  -- array of unitID

    -- initialization
    if not self.is_initialized then
        -- validation
        for i = 1, #transporters do
            local transporterID = transporters[i]
            local valid = true
            -- transporter is valid
            if not IsValidUnitID(transporterID) then
                Logger.warn("nota_michelle_intro.unloadAllTransporters", "The transporter ID [" .. transporterID .. "] is not valid.")
                valid = false
            end
            -- transporter is not dead
            local unitIsDead = UnitIsDead(transporterID)
            if unitIsDead == true or unitIsDead == nil then
                Logger.warn("nota_michelle_intro.unloadAllTransporters", "The transporter with ID [" .. transporterID .. "] is dead.") 
                valid = false
            end
            -- transporter is actually a transporter
            if valid then
                local unitDefID = GetUnitDefID(transporterID)
                if not UnitDefs[unitDefID].isTransport then
                    Logger.warn("nota_michelle_intro.unloadAllTransporters", "The unit with ID [" .. transporterID .. "] is not a transporter.")
                    valid = false
                end
            end
            -- issue unload commands if the transporter is occupied
            if valid and #GetUnitIsTransporting(transporterID) > 0 then
                local transporter_position = Vec3(GetUnitPosition(transporterID))
                GiveOrderToUnit(transporterID, CMD.UNLOAD_UNIT, {transporter_position.x, transporter_position.y, transporter_position.z}, {"shift"})
            end
        end
        
        self.is_initialized = true
    end

    -- check all success conditions
    local is_success = true
    for _, transporterID in ipairs(transporters) do
        if IsValidUnitID(transporterID) and #GetUnitIsTransporting(transporterID) > 0 then
            is_success = false
            break
        end
    end
    if is_success then
        return SUCCESS
    end

    -- check all failure conditions

    -- otherwise running

    return RUNNING
end

function Reset(self)
    self.is_initialized = false
end