function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Wait until all the given units are loaded or unloaded (depending on the parameter).",
		parameterDefs = {
			{ 
				name = "assigned_units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter assigned_units [array] - list of unitID
			--[[ local example = {
				[1] = 14945,
				[2] = 5814,
				[3] = 126450,
			}
			]]--
			{ 
				name = "loaded",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter transporter - bool
		}
	}
end

-- speed-ups
local IsValidUnitID = Spring.ValidUnitID
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitTransporter = Spring.GetUnitTransporter

function Run(self, units, parameter)
    local assigned_units = parameter.assigned_units  -- array of unitID
    local if_loaded = parameter.loaded  -- bool

    -- initialization
    if not self.is_initialized then
        -- validation
        for _, unitID in ipairs(assigned_units) do
            --    unit is valid
            if not IsValidUnitID(unitID) then
                Logger.warn("nota_michelle_intro.waitUntilLoadedOrUnloaded", "The unit ID [" .. unitID .. "] is not valid.") 
                return FAILURE
            end
            --    unit can be transported
            unitDefID = GetUnitDefID(unitID)
            if UnitDefs[unitDefID].cantBeTransported then
                Logger.warn("nota_michelle_intro.waitUntilLoadedOrUnloaded", "The unit with ID [" .. unitID .. "] cannot be transported.") 
                return FAILURE
            end
        end
        
        self.is_initialized = true
    end

    for _, unitID in ipairs(assigned_units) do
        -- running if units should be loaded but at least one is not
        if GetUnitTransporter(unitID) == nil and if_loaded then
            return RUNNING
        end
        -- running if units should be unloaded but at least one is not
        if GetUnitTransporter(unitID) ~= nil and not if_loaded then
            return RUNNING
        end 
    end
    return SUCCESS
end

function Reset(self)
    self.is_initialized = false
end